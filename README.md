# Pretty Dicey

These are just some game-related utils. Some are really helpful, some
are just little experiments that have no home elsewhere (yet?).

Think of it as a wizard's workshop; some things you try here will make
you powerful, and other things will kill you.

The manifest so far:

## mindice

A little Python command that rolls dice based on progressive
modulo. In other words, you can seed this roller with two values (or
let it choose two for you) and then get roll results derived
mathematically from the previous roll.

It aims to provide *unexpected numbers*, which just *feels* more
natural to me than blindly trusting ``/dev/random`` (although if you're
a kernel developer and truly understand ``/dev/random``, you may
disagree).


## Encounter Builder

A short document collecting philosophy and methodology of encounter
building, developed by Mike Shea on the Tome Show podcast and Matthew
Colville on his Youtube channel. I found their ideas so darned useful
that I typed them up into a formal document, which I keep in my DM
utils folder and refer to often. I guarantee, this will change your DM
prep.

To download just the information as a PDF, look in the ``dist`` directory. All
the other stuff is Docbook source code, which you don't need unless you intend
to modify the document.


## Treasure table

A table of 100 items of varying value for when your players come upon some
unscripted valuables. Roll a d100 a few times and give them their treasure.

This kind of table flourishes with collaboration, so patches welcome. The
source is in [Open OFfice](http://openoffice.org) or [Libre
Office](http://libreoffice.org) format, so it's easy to contribute.
