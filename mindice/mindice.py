#!/usr/bin/env/python3
import sys
import random
import argparse

def getOptions(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="Dirty hack of a dice roller.")
    parser.add_argument("-a", "--alpha", type=int,dest='alpha',help="First seed (set to 0 for random)")
    parser.add_argument("-o", "--omega", type=int,dest='omega',help="Second seed (set to 0 for random)")
    parser.add_argument("-d", "--die", type=int,dest='sides',help="Sides on your die. 0=random.")
    parser.add_argument("-r", "--rolls", type=int,dest='rolls',help="How many rolls to make (set to 0 for random)")
    parser.add_argument("-v", "--verbose",dest='verbose',action='store_true', help="Verbose mode.")
    options = parser.parse_args(args)
    return options

options = getOptions(sys.argv[1:])

if options.verbose:
    V=1
    print("Verbose mode on")
else:
    V=0

if not options.sides or options.sides is 0:
    options.sides = random.randrange(1,21)
else:
    pass

if not options.rolls or options.rolls is 0:
    options.rolls = random.randrange(1,21)
else:
    pass

if not options.alpha or options.alpha is 0:
    options.alpha = random.randrange(1,options.sides)
else:
    pass

if not options.omega or options.omega is 0:
    options.omega = random.randrange(1,options.sides)
else:
    pass

## done parsing

A = int(options.alpha)%int(options.sides)
O = int(options.omega)%int(options.sides)
R = int(A+O)%int(options.sides)
RET = [O,R]

def roll(S1,S2):
    A = int(S1)%int(options.sides)
    O = int(S2)%int(options.sides)
    D = int(A+O)%int(options.sides)
    RET = [O,D]
    return RET
    
n = 1
while n < options.rolls + 1:
    RET = roll(RET[0],RET[1])
    print(RET[1])
    n=n+1
